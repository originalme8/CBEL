#! /bin/bash
#
# Linux enhanced log library for shell scripting
# Author: Christopher S. Bates
#
# Allows you to easily extend the path for current user without risking overwriting the existing path
#
# Usage:
#  Source the clog and other required libraries to utilize CBEL full script.
#  Once clog and env_management have been sourced, then you can use lib_import to pull all libraries if required


# Set environment variable
set_env()
{
    SCRIPT_NAME=`basename "$0"`

    SCRIPT_DIR="$(realpath $(dirname -- ${BASH_SOURCE[0]}))"

    if [ "`whoami`" == 'root' ]; then
        SCRIPT_USER=`echo ${SUDO_USER}`
    else
        SCRIPT_USER=`whoami`
    fi    
}

# Function to run find command as root, prompt if needed, pass "-i" for case insensitive
function root_find()
{
    if [ "${1}" == "-i" ]; then
        F_ARG="-iname"
    else
        F_ARG="-name"
    fi

    if [ "`whoami`" != 'root' ]; then
        if [ `which sudo` != "" ]; then
            sudo find / ${F_ARG} "${1}" 2>/dev/null
        else
            echo "This function requires root"
            su -c "find / ${F_ARG} ${1}" 2>/dev/null
        fi
    else
        find / ${F_ARG} "${1}" 2>/dev/null
    fi

    logger -t "cenv_management" "root_find - Found ${1}"
}

# List main environment variables
list_env()
{
    echo "SCRIPT_NAME = ${SCRIPT_NAME}"
    echo "SCRIPT_DIR = ${SCRIPT_DIR}"
    echo "SCRIPT_USER = ${USER}"
    echo "CBEL_PATH = ${CBEL_PATH}"
}

# Safeuly extend $PATH variable and log new path string
function extend_path()
{
    PATH="${PATH}:${1}"
    log "New Path - ${PATH}"
}

# Import libraries from in specific directory
function lib_import() 
{
    log  " Sourcing all files in ${1}"
    local LIB_DIR="$(realpath ${1})"

    if [ -f "${LIB_DIR}" ]; then 
        LIB_DIR=`realpath $(dirname -- "${LIB_DIR}")`
    fi

    local LIBS=( " `ls ${LIB_DIR}` " )

    for lib in ${LIBS[@]}; do
            source ${LIB_DIR}/${lib}
            logger -i `basename "$0"` "${FUNCNAME[0]} - Imported ${LIB_DIR}/${lib}"
    done
}



#### CBEL Specific functions

# Import all CBEL libraries automatically or by path
function import_cbel()
{
    if [ "${1}" == '' ]; then

        CENVM="cenv_management" # This is the name of this library. It should be bundled with the rest of CBEL

        logger -t "`basename $0`" "${FUNCNAME[1]} - No path given for cbel, trying to find automatically"

        CENVM=`root_find ${CENVM}` # Use root_find function to locate this file
        
        CENVM=`realpath "${CENVM}"` # Get realpath of returned file

        if [ ! -f "${CENVM}" ]; then 
        
            logger -t "`basename "$0"`" "${FUNCNAME[1]} - Failed to locate cenv_management, exiting"
            return 2
        fi
        
        source "${CENVM}"
        CBEL_PATH=`realpath $(dirname -- "${CENVM}")`
    else

        logger -t "`basename $0`" "${FUNCNAME[1]} - sourcing cbel from defined location: "`realpath ${1}`""

        if [ -f "`realpath ${1}`" ]; then 
            CBEL_PATH=`realpath $(dirname -- "${1}")`
        elif [ -d "${1}" ]; then
            CBEL_PATH="`realpath ${1}`"
        else
            logger -t "`basename $0`" "${FUNCNAME[1]} - Unable to find path provided"
        fi 
    fi

    logger -t "`basename "$0"`" "${FUNCNAME[1]} - CBEL Path found at ${CBEL_PATH}"

    if [ ! -d "${CBEL_PATH}"  ]; then
        logger -t "`basename "$0"`" "${FUNCNAME[1]} - Unable to identify CBEL, not importing libraries"
        return 3
    fi
    
    if [ -f "${CBEL_PATH}/clog" ]; then 
        logger -t "`basename "$0"`" "${FUNCNAME[1]} - Sourcing log library"
        source "${CBEL_PATH}/clog"
    else
        logger -t "`basename "$0"`" "${FUNCNAME[1]} - unable to import clog, exiting"
        return 4
    fi

    

    lib_import "${CBEL_PATH}"
}

