#! /bin/bash

# Linux enhanced log library for shell scripting
# Author: Christopher S. Bates

###########################################################################
# Function: Set Log Level
###########################################################################
#   Usage:
#       set_log_level [options] [level]
#
#       Used to configure log levels for uses of log() function. 
#
#   Priority:
#       --console       Enable logging to user console
#
#   Level:
#       debug           Enable all logging messages
#       warn            Enable info, error, and warning messages
#       info            Enable info and error (default)
#       error           Enable error messages only
#       nolog           Disable all logging
###########################################################################
function set_log_level()
{

    while [ "$1" != "" ]; do
        case "$1" in
            --help)
                LOG_HELP=true
                break
                ;;
            --console)
                CONSOLE=true
                log "Console logging enabled"
                ;;
            debug)
                LOG_LEVEL='Debug'
                LOG_DEBUG=true
                LOG_WARN=true
                LOG_INFO=true
                LOG_ERROR=true
                ;;
            warn)
                LOG_LEVEL='Warn'
                LOG_DEBUG=false
                LOG_WARN=true
                LOG_INFO=true
                LOG_ERROR=true
                ;;
            nolog)
                log 'Logging Disabled'
                LOG_DEBUG=false
                LOG_WARN=false
                LOG_INFO=false
                LOG_ERROR=false
                ;;
            info)
                log 'Logging set to Info'
                LOG_DEBUG=false
                LOG_WARN=false
                LOG_INFO=true
                LOG_ERROR=true
                ;;
            error)
                log 'Logging set to Error'
                LOG_DEBUG=false
                LOG_WARN=false
                LOG_INFO=error
                LOG_ERROR=true
                ;;
            *)
                LOG_LEVEL='Info'
                LOG_DEBUG=false
                LOG_WARN=false
                LOG_INFO=true
                LOG_ERROR=true
        esac
        shift
    done

    case "${LOG_LEVEL}" in
        debug)
            set_traps debug
            ;;
        warn)
            set_traps warn
            ;;
        nolog)
            break
            ;;
        *)
            set_traps info
    esac

    log "Log level set to ${LOG_LEVEL}"

    if [ "${LOG_HELP}" == 'true' ]; then
        log_help
    fi


}


###########################################################################
# Function: Log
###########################################################################
#   Usage:
#       log [priority] [<message>]
#
#       Enhanced logging function to simplify debugging
#
#   Variables:
#       CONSOLE     Also log to stderr if set to true
#
#   Priority:
#       info        Standard log level (default level if left blank)
#       warn        Generate "Warning" message
#       error       Generate "error" Logging
#       debug       Generate "debug" message
###########################################################################
function log()
{
    FUNC_NAME="${FUNCNAME[1]}"

    if [ -z "${LOG_INFO}" ]; then
        LOG_INFO=true
        LOG_ERROR=true
    fi

    if [ "${SCRIPT_NAME}" == "" ]; then
        SCRIPT_NAME=`basename "$0"`
    fi

    if [ "${FUNC_NAME}" == "" ]; then
        FUNC_NAME="main"
    fi

    if [ "$2" == "" ]; then
        if [ "$1" == "--help" ]; then
            LOG_HELP=true
        else
            [ "${LOG_INFO}" == 'true' ] && LOG_LEVEL="user.info" || return
            MESSAGE="${1}"
        fi
    else
        case "$1" in
            --help)
                LOG_HELP=true
                break
                ;;
            debug)
                [ "${LOG_DEBUG}" == 'true' ] && LOG_LEVEL="user.debug" || return
                ;;
            info)
                [ "${LOG_INFO}" == 'true' ] && LOG_LEVEL="user.info" || return
                ;;
            warn)
                [ "${LOG_WARN}" == 'true' ] && LOG_LEVEL="user.warn" || return
                ;;
            error)
                [ "${LOG_ERROR}" == 'true' ] && LOG_LEVEL="user.error" || return
                ;;
            *)
                [ "${LOG_INFO}" == 'true' ] && LOG_LEVEL="user.info" || return
            esac
        
        MESSAGE="$2"
    fi
    
    if [ "${LOG_HELP}" == 'true' ]; then
        log_help
    fi
    
    if [ "${CONSOLE}" == "true" ]; then
        logger -s -i "${SCRIPT_NAME}" -p "${LOG_LEVEL}" "${FUNC_NAME} - ${MESSAGE}"
    else
        logger -i "${SCRIPT_NAME}" -p "${LOG_LEVEL}" "${FUNC_NAME} - ${MESSAGE}"
    fi
}

function set_traps()
{
    case "$1" in
        debug)
            LOG_TRAPS='debug'
            ;;
        warn)
            LOG_TRAPS='warn'
            ;;
        *)
            LOG_TRAPS='info'
    esac

    if [ "${LOG_TRAPS}" == 'debug' ]; then
        trap "{ log 'SIGILL Detected, look for core dump'; exit 0;}" SIGILL
        trap "{ log 'Abort called exiting'; exit 0;}" SIGABRT

        LOG_TRAPS='warn'

    fi

    if [ "${LOG_TRAPS}" == 'warn' ]; then

        
        LOG_TRAPS='info'
    fi

    trap "{ log 'Script interupted with keyboard'; exit 0;}" SIGINT SIGQUIT SIGQUIT
    trap "{ log 'Hangup detected, controlling process may have died'; exit 0;}" SIGHUP
    trap "{ log 'External kill detected something stopped the process'; exit 0;}" SIGKILL
    trap "{ log 'External termination detected something stopped the process'; exit 0;}" SIGTERM
    trap "{ log 'Stop called by tty something stopped the process'; exit 0;}" SIGSTP


}

function log_help()
{
    echo '
    Functions:
        This library containsn the following functions, detailed descriptions for each can be found below:
            - set_log_level()
            - log()

    ##########
    # set_log_Level
    ##########

    Usage:
        set_log_level [options] [level]

        Used to configure log levels for uses of log() function

    Priority:
        --console       Enable logging to user console
        --help          Print this help message

    Level:
        debug           Enable all logging messages
        warn            Enable info, error, and warning messages
        nolog           Disable all logging

    ###########
    # log()
    ###########
    Usage:
        log [priority] [<message>]

    Call to generate log message based on set_log_level settings

    Variables:
        CONSOLE     If set to "true", then log to stderr (user console)
                    This can be set via `set_log_level --console` or by setting the CONSOLE variable to true in the script    
    Priority:
        info        Standard log level (default level if left blank)
        warn        Generate "Warning" message
        error       Generate "error" Logging
        debug       Generate "debug" message
    
    '
    exit 0
}
