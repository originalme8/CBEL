# CBBEL 
- Chris Bates' - BASH Enhancement Libraries (CBBEL)


## Library File List

- [environment_setup](#environmentsetup)
- [log](#log) - Enhanced logging library that easily allows you to easily implement logging into your bash scripts

### environment_setup

#### Description

Simple library that can be used to setup a basic environment for easier scripting. 

Sets the following global script variables:

- USER - Username of user who executed the script
- SCRIPT_NAME - Name of the script file
- SCRIPT_DIR - Directory where the main script resides

#### Usage

1. Import file by calling "source \<path_to_file>"
2. Call "get_env" to populate script variables

##### Example

```shell
source ./lib/environment_setup

get_env

list_env

```

#### Functions

##### Main Functions

- [get_env\(\)](#getenv) - Poulates all environment variables contained in library
- [list_env\(\)](#listenv) - Lists environment set by this library


###### get_env

##### list_env

### log

#### Description

The CBEL log library allows for simple logging configuration within bash scripts.

#### Usage

1. Import file by calling "source \<path_to_file>"
2. Call "set_log_level" to set global log level
3. Call "log" to generate a log entry

##### Example

```shell
#!/bin/bash
source ./lib/log
set_log_level --console info
log "Hello world"
```

#### Functions

This library containsn the following functions, detailed descriptions for each can be found below:

##### Main Functions
- [set_log_level\(\)](#se) - Call to set the global log level, or enable console logging
- [log\(\)](#log) - Call when you want to generate a log entry

###### set_log_Level

```text
Usage:
    set_log_level [options] [level]

Used to configure log levels for uses of log() function

Priority:
    --console       Enable logging to user console
    --help          Print this help message
Level:
    debug           Enable all logging messages
    warn            Enable info, error, and warning messages
    nolog           Disable all logging```
```

###### log

```text
Usage:
    log [priority] [<message>]

Call to generate log message based on set_log_level settings

Variables:
    CONSOLE     If set to "true", then log to stderr (user console)
                This can be set via `set_log_level --console` or by setting the CONSOLE variable to true in the script
Priority:
    info        Standard log level (default level if left blank)
    warn        Generate "Warning" message
    error       Generate "error" Logging
    debug       Generate "debug" message
```

###### Additional Functions
- log_help - Cannot call directly, access with "--help" option to return help dialogue